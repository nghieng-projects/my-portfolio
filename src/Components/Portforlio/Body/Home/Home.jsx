import React from 'react';
import './Home.scss';
import ball1 from '../../../../Assets/ball1.png';
import ball2 from '../../../../Assets/ball2.png'
import ball3 from '../../../../Assets/ball3.png'
import Animation from "../Animation";

const Home = () => {
    return (
        <div className='home'>
            <div className='boxHome'>
                <div className='content'>
                    {/*<div className='title font-effect-shadow-multiple'>*/}
                        <div className='title'>
                        <p>Hello everyone!</p>
                        <p>My name is Nguyen.</p>
                        <p>I'm here to be improve myself and shining. Today be better than i was yesterday.</p>
                    </div>

                    <div className='intro'>
                        <p>I build UI/UX for website.</p><br/>
                        <p>In 2013, I graduated from college school in accounting and I have nearly 5 years
                            of experience from sales, leader, to administrator.
                            So, front-end developer is my left branch.</p><br/>
                        <p>I have just contacted with this position for nearly two years, self-study at home on internet
                            and youtube channel.</p><br/>
                        <p>With my age, people think it is too late to study again, I used to think like that. But,
                            I tried my best to learn and find out which skills is necessary for this position ?</p><br/>
                        <p>HTML, CSS, JS, REACTJS.</p><br/>
                        <p>Everything is still the basics, read documents, make small demos... So I need a professional
                            environment to let me know how to applies everything I learned into real projects.</p><br/>
                        <p>I am sociable, friendly, interested in exploring and eager to learn new things. I hope that
                            you can give me a chance to work as an internship at your company, I will improve myself and
                            make a meaningful contribution to your research effort.</p>
                    </div>

                </div>
                <div className='fly'>
                    <Animation src={ball1} style={'default ball1'}/>
                    <Animation src={ball2} style={'default ball2'}/>
                    <Animation src={ball3} style={'default ball3'}/>
                </div>
            </div>

        </div>
    )
}
export default Home