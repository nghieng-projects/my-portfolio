import React from 'react';
import './AnimationImg.scss';
const AnimationImg = ({src,style}) => {

    return(
        <img src={src} className={style} alt=""/>
    )

}
export default AnimationImg