import React from "react";
import ReactOwlCarousel from "react-owl-carousel";
import 'owl.carousel/dist/assets/owl.carousel.min.css';
import 'owl.carousel/dist/assets/owl.theme.default.min.css';
import './Work.scss';
import 'animate.css';
import pic0 from "../../../../Assets/klassy.png";
import pic1 from "../../../../Assets/simplegame.png";
import pic2 from "../../../../Assets/todolist.png";
import pic4 from "../../../../Assets/yarnshopping.png";

const Work = () => {
    const optionsOwl = {
        className:'owl-theme',
        items:'3',
        autoPlay:'true',
        autoHeight: false,
        mouseDrag:'true',
        dots:'true',
        responsive: {
            0: {
                items: 1,
            },
            400: {
                items: 1,
            },
            600: {
                items: 2,
            },
            700: {
                items: 2,
            },
            800:{
                items: 2,
            },
            1000: {
                items: 3,
            }
        },
    }
    return (
        <div className='work'>
            <div className='boxWork'>
                <div className='parts'>
                    <h3>My Projects</h3>
                    <ReactOwlCarousel {...optionsOwl}>
                        <div className="product">
                            <div className="info">
                                <a href="https://calendartodolist-kmj7afvw9-nguyen-projects.vercel.app/" target="_blank"><img src={pic2} alt=""/></a>
                                <a href="https://calendartodolist-kmj7afvw9-nguyen-projects.vercel.app/" target="_blank">
                                    <div className="name">
                                        <strong>Calendar Todolist App</strong>
                                    </div>
                                </a>
                            </div>
                            <div className="description">
                                <div>This project I was make at the beginning when I start to learn ReactJs and API</div>
                                <div>It's a Todo list app, you can add todo items, you can mark an item is completed when you finished and you can also delete items</div>
                                <div className="row"><strong>Technologies</strong>: ReactJs, HTML, SCSS, json-server, fetchApi</div>
                                <div className="row">
                                    <strong>Support responsive</strong>
                                </div>
                            </div>
                        </div>
                        <div className="product">
                            <div className="info">
                                <a href="https://yarnshopping-2turwzmw2-nguyen-projects.vercel.app/" target="_blank"><img src={pic4} alt=""/></a>
                                <a href="https://yarnshopping-2turwzmw2-nguyen-projects.vercel.app/" target="_blank">
                                    <div className="name">
                                        <strong>Yarn Shopping</strong>
                                    </div>
                                </a>
                            </div>
                            <div className="description">
                                <div>It's a shopping website where you can buy yarns. It have 5 pages to display the list of product, and 1 product detail page to display the details of a selected product. You buy and checkout in the Cart icon on top-right, then if your total cost is above $50, you will get free ship.</div>
                                <div>This is <a style={{color:"blue"}} href="https://yarn-shopping.nguyenphan.info/product" target={"_blank"}>Admin link</a>, here we can manage products. </div>
                                <div className="row"><strong>Technologies</strong>: HTML, SCSS, ReactJs, FetchAPI, LocalStorage, json-server</div>
                                <div className="row">
                                    <strong>Support responsive</strong>
                                </div>
                            </div>
                        </div>
                        <div className="product">
                            <div className="info">
                                <a href="https://klassy-dbtcgu7q3-nguyen-projects.vercel.app/" target="_blank"><img src={pic0} alt=""/></a>
                                <a href="https://klassy-dbtcgu7q3-nguyen-projects.vercel.app/" target="_blank">
                                    <div className="name">
                                        <strong>Klassy Cafe Template</strong>
                                    </div>
                                </a>
                            </div>
                            <div className="description">
                                <div>This project I was make at the beginning when I start to learn HTML and CSS and ReactJs</div>
                                <div className="row"><strong>Technologies</strong>: HTML, SCSS, ReactJs</div>
                                <div className="row">
                                    <strong>Support responsive</strong>
                                </div>
                            </div>
                        </div>
                        <div className="product">
                            <div className="info">
                                <a href="https://math-game.nguyenphan.info/" target="_blank"><img src={pic1} alt=""/></a>
                                <a href="https://math-game.nguyenphan.info/" target="_blank">
                                    <div className="name">
                                        <strong>Math Game</strong>
                                    </div>
                                </a>
                            </div>
                            <div className="description">
                                <div>This project was make when I start to learn Javascript. I make this game for kids, they can play by clicks the <strong>Start</strong> button, and then follow the rule of the game they play.</div>
                                <div><strong>Technologies</strong>:Javascript, HTML, CSS</div>
                                <div>
                                    <strong>Support responsive</strong>
                                </div>
                            </div>
                        </div>
                    </ReactOwlCarousel>
                </div>
                <div className='parts'>
                    <p className='endText' style={{textAlign: 'center'}}>Thank you for taking the time to my
                        projects.</p>
                </div>
            </div>

        </div>
    )
}
export default Work
