import React from 'react';
import './Story.scss';
import mobile from '../../../../Assets/mobile.png'

const Story = () => {
    return (
        <div className='story'>
            <div className='contentBox'>
                <div className='bodyStory'>
                    <div className='parts'>
                        <img src={mobile} alt="" className='mobile'/>
                    </div>
                    <div className='parts'>
                        <div className='conversation'>
                            <p className='title'>A corner of my life.</p>
                            <div className='openSences'>
                                <p>Once upon a time, have a pretty girl lives in a busy town ( Yup! Of course HCM city ),
                                    she work as an hard working staff everyday. Day by day, time is gone by, she felt
                                    not happy with her career in that time anymore. She has changed 2 or 3 difference positions,
                                    but it didn't worked, she was nervous. And one day ... </p>
                            </div>
                            <div className='mainStory'>
                                <div className='box'>
                                    <div className='greyText'>
                                        <p className='text1'>Boring! I don't know, what should i do?</p>
                                    </div>
                                    <div className='greenText'>
                                        <p className='text2'>Hey! Good at Microsoft Excel, that's mean you can code!</p>
                                    </div>
                                    <div className='greyText'>
                                        <p className='text3'>What?</p>
                                    </div>
                                    <div className='greenText'>
                                        <p className='text4'>I'll help you, read documents first, practice more and u'll be
                                            a coder.</p>
                                    </div>
                                    <div className='greyText'>
                                        <p className='text5'>Are you kidding me? ... Ok! If u trick me, i'll kill u.</p>
                                    </div>
                                </div>
                            </div>
                            <div className='endScenes'>
                                <p>According i know, util now, <span style={{textDecoration: 'underline'}}>he still alive</span>.
                                </p>
                                <p>The end.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}
export default Story