import React from 'react';
import './Body.scss';
import {Routes,Route} from 'react-router-dom';

import Work from "./Work";
import Story from "./Story";
import Home from "./Home";
const Body = () => {
    return(
        <div className='body'>
            <Routes>
                <Route path='/story' element={<Story />}/>
                <Route path='/work' element={<Work />}/>
                <Route path='/' element={<Home/>}/>
            </Routes>
        </div>
    )
}
export default Body