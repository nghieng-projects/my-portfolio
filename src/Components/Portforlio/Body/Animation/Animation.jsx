import React, {useEffect, useState} from 'react';
import './Animation.scss';
import AnimationImg from "../components/AnimationImg";
import img1 from '../../../../Assets/sunflower.jpg';
import styled, {keyframes} from "styled-components";

const move = props => keyframes`
      0% {
        top: ${props.arrTop[0]}em;
        left: ${props.arrLeft[0]}em;
      }
      20% {
        top: ${ props.arrTop[1]}em;
        left: ${props.arrLeft[1]}em;
      }
      40% {
        top: ${ props.arrTop[2]}em;
        left: ${props.arrLeft[2]}em;
      }
      60% {
        top: ${ props.arrTop[3]}em;
        left: ${props.arrLeft[3]}em;
      }
      80% {
        top: ${props.arrTop[4]}em;
        left: ${props.arrLeft[4]}em;
      }
      100% {
        top: ${ props.arrTop[5]}em;
        left: ${props.arrLeft[5]}em;
      }
    `
const StyledAnimation = styled.div`
      width: 7em;
      height: auto;
      border-radius: 50%;
      visibility: ${props=>props.playing ? "visible" : "hidden"};
      position: absolute;
      animation: ${props => move(props)} ease-in-out alternate-reverse infinite;
      animation-duration: 15000ms;
    `
const Top = (min,max)=>{
    const num = Math.floor(Math.random() * (max - min) + min);
    return num
}
const Left = (min,max)=>{
    const num = Math.floor(Math.random() * (max - min) + min);
    return num
}

const Animation = ({src,style}) => {
    const [playing,setPlaying] = useState(false)
    const arrTop = []
    const arrLeft = []
    while (arrTop.length < 6) {
        /*const randomTop = Math.floor(Math.random() * 30)
        const randomLeft = Math.floor(Math.random() * 90)*/
        const randomTop = Top(10,35);
        const randomLeft = Left(60,80);
        arrTop.push(randomTop)
        arrLeft.push(randomLeft)
    }
    useEffect(()=>{
        setTimeout(()=>{ setPlaying(true) },500)
    }, [])

    return (
        <div className='animation'>
            <StyledAnimation playing={playing} arrTop={arrTop} arrLeft={arrLeft}>
                <AnimationImg src={src} style={style}/>
            </StyledAnimation>
        </div>
    )

}
export default Animation