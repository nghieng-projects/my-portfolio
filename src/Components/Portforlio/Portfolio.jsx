import React from 'react';
import './Portfolio.scss';
import Header from "./Header";
import Body from "./Body";


const Portfolio = () => {
    return (
        <div className='portfolio'>
            <Header/>
            <Body />
        </div>
    )
}
export default Portfolio