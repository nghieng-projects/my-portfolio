import './App.css';
import {BrowserRouter} from "react-router-dom";
import Portfolio from "./Components/Portforlio";

const App = () => {

    return (
        <div className="App">
            <BrowserRouter>
                <Portfolio />
            </BrowserRouter>
        </div>
    );
}

export default App;
